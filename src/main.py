import flac3d_setup  # noqa
from modelling import config
from modelling import managers

config.initialize('config.yml')


class MyModel(managers.CaseManager):
    """
    Available CaseManager attributes:
        self.case:
            case taken from the git branch name verbatim
        self.case_name:
            case with slashes replaced with dashes - primarily for filenames

        self.git_directory:
            directory of the git repository
        self.local_base_directory:
            working directory on local computer
        self.shared_base_directory:
            network directory where input/output is stored

        self.input_directory:
            input directory on network drive
        self.output_directory:
            output directory on network drive
        self.input_directory_long:
            input directory on network drive formatted to support
            paths longer than 248 characters
        self.output_directory_long:
            output directory on network drive formatted to support
            paths longer than 248 characters

        self.output_directories:
            dictionary of specific output_directories on the network drive
        self.output_directories_long:
            dictionary of specific output_directories on the network drive
            formatted to support paths longer than 248 characters

        self.parameters:
            list of calibration/sensitivity parameters
        self.realizations:
            generator to be used for iterating through realizations

    Available CaseManager methods:
        self.get_input_file_path(*path_list, processed=False, long=False):
            Returns the full input file path by specifying the path relative
            to the input folder.
        self.get_output_file_path(directory, name, extension, long=False):
            Returns the full output file path (including case and parameter
            names) by specifying the directory key as defined in config.yml
            the file name and the file extension.
    """

    def __init__(self):
        """Put model initialization code in here"""

        super(MyModel, self).__init__()

    def run(self):
        """Put model run code in here"""

        pass


if __name__ == '__main__':
    model = MyModel()
    model.run()
