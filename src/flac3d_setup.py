import subprocess
import sys
import os

module_info = subprocess.check_output(
    ['python', '-m', 'pip', 'show', 'golder'])
location_info = [
    line for line in module_info.split('\n') if
    line.startswith('Location')]

if len(location_info) > 0:
    location = location_info[0].split('Location:')[-1].strip()
else:
    location = None

golder_path = os.path.join(location, 'golder')
if golder_path not in sys.path:
    sys.path.append(golder_path)
